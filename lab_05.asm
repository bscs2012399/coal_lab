
; You may customize this and other start-up templates; 
; The location of this template is c:\emu8086\inc\0_com_template.txt

.model small
.stack 100h
.data
     var1 db  "Samrah Durrani $"
     var2 db 0Ah,0Dh,"Huzaifa Ali Khan\" 
.code
main proc
        
        mov ax,@data
        mov ds,ax
        mov dl,offset var1
        mov ah,09
        int 21h
        mov ah,4ch
        mov bl, offset var2
        mov dl,bl
        mov ah,09
        int 21h  
        mov dl,36
        mov ah,02
        int 21h
      
        
        
    main endp
end main

